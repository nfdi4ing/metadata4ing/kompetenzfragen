# Wenn es eine allwissende Datenbank mit gut erschlossenen Forschungsdaten aus Ihrer Domäne gäbe - welche Fragen würden Sie an sie stellen?

Mit den Kompetenzfragen möchte die Gruppe Metadata4Ing die ingenieurswissenschaftlichen Bedarfe an eine Metadatenbeschreibung von Forschungsdaten konkretisieren. Wir möchten insbesondere Domänenexperten zur Mitwirkung einladen. Eigene Beiträge können Sie nach einem Log-In mit Ihrem DFN-AAI-Single-Sign-On-Account oder einem GitHub-Account gestalten.

## Neue Kompetenzfragen hinzufügen
Ergänzen Sie Ihre Kompetenzfrage als Issue.

New! [Find our How-tos as screencasts](https://git.rwth-aachen.de/nfdi4ing/metadata4ing/kompetenzfragen/-/wikis/How-to-add-a-new-competency-question) (WIP)

- Nehmen Sie die Kompetenzfrage im Titel des Issues auf.
- Fügen Sie dem Issue Labels hinzu, um es einer Gruppe von anderen Fragen zuordnen zu können:
    - das Label "CompetencyQuestion", um es als Kompetenzfrage zu kennzeichnen
    - ein Label zum Status: "proposed" oder "inDiscussion"
    - wenn gemwünscht weitere Label zur thematischen Einordnung: z.B. "HPC", "Method"

## Kompetenzfragen bewerten
Zustimmung oder Ablehnung einer Frage können schnell über den "Daumen hoch"- oder "Daumen runter"-Button ausgedrückt werden. 

## Kompetenzfragen diskutieren
Bei detaillierterem Feedback oder Fragen zu einer Kompetenzfrage können Sie die Kommentierungsfunktionen im Issue nutzen, oder auf vorhandene Kommentare antworten.
